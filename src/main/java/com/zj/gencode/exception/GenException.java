package com.zj.gencode.exception;

/**
 * @author xi.yang
 * @create 2018-10-16 15:45
 **/
public class GenException extends RuntimeException {
    private String message;

    public GenException(String message) {
        super(message);
    }
}
