package com.zj.gencode;

import com.zj.gencode.config.GenConfig;
import com.zj.gencode.model.PropField;
import com.zj.gencode.model.PropTable;
import com.zj.gencode.type.FieldType;
import com.zj.gencode.type.FileType;
import com.zj.gencode.type.SqlType;
import com.zj.gencode.utils.excel.ExcelUtil;
import com.zj.gencode.utils.velocity.VmUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Mr. xi.yang<br/>
 * @version V1.0 <br/>
 * @description: 生成javabean、mybatis配置、sql <br/>
 * @date 2017-09-27 上午 9:33 <br/>
 */
@Slf4j
public class Client {
    private static final Logger logger = LoggerFactory.getLogger(Client.class);
//    初始化获取配置并校验参数是否完整
    private static GenConfig genConfig = GenConfig.getInstance();
    public static void main(String[] args) {
//        获取excel配置
        List<PropTable> sqlTableList = getSqlTables();
//        完善数据
        compeleteDatas(sqlTableList);
//        生成sql文件
        Map<String,Object> map = new HashedMap();
        map.put("data",sqlTableList);
        VmUtil.vmToFile("sql.vm",map,genConfig.getFileOutPath() +File.separator+"init.sql");
//        生成java文件
        for (PropTable propTable : sqlTableList) {
            Map<String,Object> tempMap = new HashedMap();
            tempMap.put("data", propTable);
            for (FileType fileType : FileType.values()) {
                printOutVmFile(fileType, tempMap, propTable);
            }
        }
    }

    private static void printOutVmFile(FileType fileType, Map<String, Object> tempMap, PropTable propTable) {
        VmUtil.vmToFile(fileType.name().toLowerCase()+".vm", tempMap
                , genConfig.getFileOutPath() + File.separator + fileType.name().toLowerCase() + File.separator + propTable.getToName() + fileType.getFileSuffix());
    }

    /**
     * 完善数据
     * @param sqlTableList
     */
    private static void compeleteDatas(List<PropTable> sqlTableList) {
        String createTime = DateTime.now().toString("yyyy-MM-dd HH:mm:ss");
        for (PropTable propTable : sqlTableList) {
            propTable.setPackageName(genConfig.getToPackage());
            propTable.setCreateTime(createTime);
            propTable.setToName(underline2Camel(propTable.getTableName(),false));
            propTable.setLowerToName(underline2Camel(propTable.getTableName(),true));
            List<PropField> propFields = propTable.getPropFields();
            for (PropField propField : propFields) {
                String fieldName = underline2Camel(propField.getSqlFieldName(),true);
                propField.setFieldName(fieldName);
                propField.setFirstUpFieldName(underline2Camel(propField.getSqlFieldName(),false));
                processFieldType(propField);
                if(propField.isPrimary()){
                    propTable.setPrimaryField(propField);
                }
            }
        }
    }

    private static void processFieldType(PropField propField) {
        if(FieldType.ID_ENUM.equals(propField.getFieldType()) || FieldType.JAVA_BEAN.equals(propField.getFieldType())){
            propField.setFieldTypeString(propField.getFirstUpFieldName());
            return;
        }
        if(propField.getFieldType() == null){
            propField.setFieldType(propField.getSqlType().getFieldType());
            if(FieldType.BOOLEAN.equals(propField.getFieldType())){
                propField.setGetMethodTarget("is");
            }
            propField.setFieldTypeString(underline2Camel(propField.getFieldType().toString(),false));
            return;
        }
        propField.setFieldTypeString(underline2Camel(propField.getFieldType().toString(),false));
    }

    /**
     * 下划线转驼峰法
     * @param line 源字符串
     * @param smallCamel 大小驼峰,是否为小驼峰
     * @return 转换后的字符串
     */
    private static String underline2Camel(String line,boolean smallCamel){
        if(line==null||"".equals(line)){
            return "";
        }
        StringBuffer sb=new StringBuffer();
        Pattern pattern=Pattern.compile("([A-Za-z\\d]+)(_)?");
        Matcher matcher=pattern.matcher(line);
        while(matcher.find()){
            String word=matcher.group();
            sb.append(smallCamel&&matcher.start()==0?Character.toLowerCase(word.charAt(0)):Character.toUpperCase(word.charAt(0)));
            int index=word.lastIndexOf('_');
            if(index>0){
                sb.append(word.substring(1, index).toLowerCase());
            }else{
                sb.append(word.substring(1).toLowerCase());
            }
        }
        return sb.toString();
    }
    /**
     * 驼峰法转下划线
     * @param line 源字符串
     * @return 转换后的字符串
     */
    private static String camel2Underline(String line){
        if(line==null||"".equals(line)){
            return "";
        }
        line=String.valueOf(line.charAt(0)).toUpperCase().concat(line.substring(1));
        StringBuffer sb=new StringBuffer();
        Pattern pattern=Pattern.compile("[A-Z]([a-z\\d]+)?");
        Matcher matcher=pattern.matcher(line);
        while(matcher.find()){
            String word=matcher.group();
            sb.append(word.toUpperCase());
            sb.append(matcher.end()==line.length()?"":"_");
        }
        return sb.toString();
    }

    /**
     * 从excel获取数据
     * @return
     */
    private static List<PropTable> getSqlTables() {
        List<PropTable> result = new ArrayList<>();
        Map<String,List<Map<String, Object>>> maps = ExcelUtil.getAllExcel2Maps(genConfig.getFileExcelPath());
        for (String sheetName : maps.keySet()) {
            if ("items".equalsIgnoreCase(sheetName)) {
                continue;
            }
            String tabelContent = StringUtils.substring(sheetName,0,sheetName.indexOf("_"));
            String tabelName = StringUtils.substring(sheetName,sheetName.indexOf("_")+1,sheetName.length());
            PropTable propTable = new PropTable();
            propTable.setTableName(tabelName);
            propTable.setContent(tabelContent);
            List<Map<String,Object>> props = maps.get(sheetName);
            List<PropField> list = new ArrayList<>();
            for (Map<String, Object> prop : props) {
                PropField propField = new PropField();
                String name = String.valueOf(prop.get("字段名"));
                propField.setSqlFieldName(name);
                String type = String.valueOf(prop.get("类型"));
                propField.setSqlType(SqlType.getSqlTypeByName(type));
                int length = getIntDefault(prop.get("长度"));
                propField.setFieldLength(length);
                boolean allowNull = getBooleanDeDefault(prop.get("允许空"));
                propField.setAllowNull(allowNull);
                String defaultVal = String.valueOf(prop.get("默认值"));
                if(StringUtils.isNotBlank(defaultVal)){
                    propField.setDefaultValue(defaultVal);
                }
                String indexType = String.valueOf(prop.get("索引类型"));
                if(StringUtils.isNotBlank(indexType)){
                    if(indexType.contains("主键")){
                        propField.setPrimary(true);
                        propTable.setPrimaryKey(name);
                    }
                    if(indexType.contains("自增")){
                        propField.setAutoIncrement(true);
                    }
                    if(indexType.contains("索引")){
                        propField.setIndex(true);
                    }
                }
                String content = String.valueOf(prop.get("说明"));
                propField.setContent(content);
                String javaType = String.valueOf(prop.get("Java类型"));
                propField.setFieldType(FieldType.getFieldTypeByName(javaType));
                list.add(propField);
            }
            propTable.setPropFields(list);
            result.add(propTable);
        }
        return result;
    }

    private static int getIntDefault(Object obj) {
        if(null == obj){
            return 0;
        }
        try {
            return Integer.parseInt(obj+"");
        }catch (Exception e){
            return 0;
        }
    }
    private static boolean getBooleanDeDefault(Object obj) {
        if(null == obj){
            return false;
        }
        if(StringUtils.isBlank(obj+"")){
            return false;
        }
        if("0".equals(obj+"")){
            return false;
        }
        if("否".equals(obj+"")){
            return false;
        }
        return true;
    }


}
