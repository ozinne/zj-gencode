package com.demo;

/**
 * 通用的带id枚举接口
 * 项目中enum实现该接口并在mybaties配置中加入统一的拦截器
 * <typeHandler javaType="com.zj.test.DemoType" handler="com.zj.gencode.dependency.IdEnumHandler" />
 **/
public interface IdEnum {
    Integer getId();
}
